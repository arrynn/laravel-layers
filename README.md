## Installation

1. Place the infrastructure source code to the `lib` directory in your project

2. Register namespace in autoloader.
   
    `composer.json`
    
    ```
    "autoload": {
        "psr-4": {
            "App\\": "app/",
            "Arrynn\\": "lib/Arrynn"
        },
    }
    ```

3. Register service provider

    `config/app.php`
    
    ```
    [
        \Arrynn\Layers\LayersServiceProvider::class,
    ]
    ```

4. Generator usage:

    ```
    php artisan component:generate <dir> <componentName> <rootNamespace>
    ```

    ```
    ------- Arguments -------
    dir             The directory in which the component will be placed (relative to app dir)
    componentName   Name of the generated component (Plural is used, e.g. Consultations )
    rootNamespace   The root namespace of the component 
    ```    
        