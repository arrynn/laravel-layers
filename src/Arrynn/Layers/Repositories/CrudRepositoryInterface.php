<?php declare(strict_types=1);

namespace Arrynn\Layers\Repositories;

/**
 * Interface CrudRepositoryInterface
 * @package Arrynn\Layers\Repositories
 */
interface CrudRepositoryInterface
{
    /**
     * Returns all models of given context
     *
     * @return mixed
     */
    public function all();

    /**
     * Finds a model by primary key
     *
     * @param $key
     * @return mixed
     */
    public function get($key);

    /**
     * Stores new model
     *
     * @param $model
     * @return mixed
     */
    public function create($model);

    /**
     * Updates given model
     *
     * @param $model
     * @return mixed
     */
    public function update($model);

    /**
     * Deletes model
     *
     * @param $key
     * @return mixed
     */
    public function delete($key);

}