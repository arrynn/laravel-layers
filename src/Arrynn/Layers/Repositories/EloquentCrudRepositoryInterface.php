<?php declare(strict_types=1);

namespace Arrynn\Layers\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Interface EloquentCrudRepositoryInterface
 * @package Arrynn\Layers\Repositories
 */
interface EloquentCrudRepositoryInterface
{

    /**
     * Returns all models of given context
     *
     * @return LengthAwarePaginator
     */
    public function all(): LengthAwarePaginator;

    /**
     * Finds a model by primary key
     *
     * @param $key
     * @return Model
     */
    public function get($key): Model;

    /**
     * Stores new model
     *
     * @param $model
     * @return Model
     */
    public function create($model): Model;

    /**
     * Updates given model
     *
     * @param $model
     * @return Model
     */
    public function update($model): Model;

    /**
     * Deletes model
     *
     * @param $key
     */
    public function delete($key): void;

}