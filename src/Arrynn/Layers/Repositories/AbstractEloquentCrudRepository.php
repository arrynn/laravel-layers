<?php declare(strict_types=1);

namespace Arrynn\Layers\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class AbstractEloquentCrudRepository
 * @package Arrynn\Layers\Repositories
 */
abstract class AbstractEloquentCrudRepository implements EloquentCrudRepositoryInterface, CrudRepositoryInterface
{
    /**
     * @var Model|string $context model context
     */
    protected $context;

    /**
     * Returns model context
     *
     * @return Model context model
     */
    abstract protected function getContext(): Model;

    /**
     * AbstractEloquentCrudRepository constructor.
     */
    public function __construct()
    {
        $this->context = $this->getContext();
    }


    /**
     * {@inheritDoc}
     */
    public function all(): LengthAwarePaginator
    {
        return $this->context::paginate($this->context->perPage);
    }

    /**
     * {@inheritDoc}
     */
    public function get($key): Model
    {
        return $this->context::findOrFail($key);
    }

    /**
     * {@inheritDoc}
     */
    public function create($model): Model
    {
        $model->save();
        return $model;
    }

    /**
     * {@inheritDoc}
     */
    public function update($model): Model
    {
        $model->save();
        return $model;
    }

    /**
     * {@inheritDoc}
     */
    public function delete($key): void
    {
        $model = $this->context::findOrFail($key);
        $model->delete();
    }
}