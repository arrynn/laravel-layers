<?php declare(strict_types=1);

namespace Arrynn\Layers;

use Arrynn\Layers\Commands\ComponentGenerator\ComponentGenerator;
use Illuminate\Support\ServiceProvider;

/**
 * Class LayersServiceProvider
 * @package App\Arrynn\Layers
 */
class LayersServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                ComponentGenerator::class,
            ]);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}