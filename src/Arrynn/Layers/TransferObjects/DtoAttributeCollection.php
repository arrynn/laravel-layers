<?php
declare(strict_types=1);

namespace Arrynn\Layers\TransferObjects;

use Arrynn\Layers\TransferObjects\Contracts\ResolvableDtoInterface;

/**
 * Class DtoAttributeCollection
 * @package Arrynn\Layers\TransferObjects
 *
 * @see DtoAttributeCollectionBuilder for usage
 */
class DtoAttributeCollection
{

    /**
     * @var DtoAttribute[]
     */
    private $attributes = [];

    /**
     * Returns the attribute list
     *
     * @return DtoAttribute[]
     */
    public function all()
    {
        return $this->attributes;
    }

    /**
     * Adds an attribute to the collection
     *
     * @param DtoAttribute $attr
     */
    public function add(DtoAttribute $attr): void
    {
        $this->attributes[] = $attr;
    }

    /**
     * Creates an associative array from attributes
     *
     * @param ResolvableDtoInterface $dto
     * @return array
     */
    public function getAssocAttributeArray(ResolvableDtoInterface $dto)
    {
        $array = [];
        foreach ($this->attributes as $attribute) {
            $attrName = $attribute->getName();
            if (isset($dto->$attrName)) {
                $array[$attrName] = $dto->$attrName;
            } else {
                $array[$attrName] = null;
            }
        }
        return $array;
    }
}