<?php
declare(strict_types=1);

namespace Arrynn\Layers\TransferObjects\Builder;

use Arrynn\Layers\TransferObjects\DtoAttribute;
use Arrynn\Layers\TransferObjects\DtoAttributeCollection;

/**
 * Class DtoAttributeCollectionBuilder
 * @package Arrynn\Layers\TransferObjects\Builder
 */
class DtoAttributeCollectionBuilder
{

    /**
     * @var DtoAttributeCollection $collection
     */
    private $collection;

    /**
     * DtoAttributeCollectionBuilder constructor.
     */
    private function __construct()
    {
        $this->collection = new DtoAttributeCollection();
    }

    /**
     * Create new instance of the builder
     *
     * @return DtoAttributeCollectionBuilder
     */
    public static function create(): self
    {
        return new self();
    }

    /**
     * Adds a primitive attribute
     *
     * @param string $name
     * @return DtoAttributeCollectionBuilder
     * @throws \Arrynn\Layers\TransferObjects\Exceptions\DtoSettingException
     */
    public function addPrimitive(string $name): self
    {
        $attr = new DtoAttribute($name);
        $this->collection->add($attr);
        return $this;
    }

    /**
     * Adds a complex attribute according to given class name
     *
     * @param $name
     * @param $class
     * @return DtoAttributeCollectionBuilder
     * @throws \Arrynn\Layers\TransferObjects\Exceptions\DtoSettingException
     */
    public function addComplex(string $name, string $class): self
    {
        $attr = new DtoAttribute($name, DtoAttribute::TYPE_COMPLEX, $class);
        $this->collection->add($attr);
        return $this;
    }

    /**
     * Adds an array of complex attributes
     *
     * @param string $name
     * @param string $class
     * @return DtoAttributeCollectionBuilder
     * @throws \Arrynn\Layers\TransferObjects\Exceptions\DtoSettingException
     */
    public function addComplexArray(string $name, string $class): self
    {
        $attr = new DtoAttribute($name, DtoAttribute::TYPE_COMPLEX, $class, true);
        $this->collection->add($attr);
        return $this;
    }

    /**
     * Builds the collection
     *
     * @return DtoAttributeCollection
     */
    public function build(): DtoAttributeCollection
    {
        return $this->collection;
    }
}