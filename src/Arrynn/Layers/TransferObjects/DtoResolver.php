<?php
declare(strict_types=1);

namespace Arrynn\Layers\TransferObjects;

use Arrynn\Layers\TransferObjects\Contracts\ResolvableDtoInterface;

/**
 * Class DtoResolver
 * @package Arrynn\Layers\TransferObjects
 */
class DtoResolver
{
    /**
     * Resolves a DTO from associative array
     *
     * @param array $array
     * @param ResolvableDtoInterface $dto
     * @return ResolvableDtoInterface
     */
    public static function fromArray(array $array, ResolvableDtoInterface $dto): ResolvableDtoInterface
    {
        $attrCol = $dto::getAttributeCollection();
        $attrs = $attrCol->all();

        foreach ($attrs as $attribute) {
            $item = self::findByKey($array, $attribute->getName());
            if (!is_null($item)) {
                self::resolveAttribute($attribute, $item, $dto);
            }
        }

        return $dto;
    }

    /**
     * Converts a DTO to an associative array
     *
     * @param ResolvableDtoInterface $dto
     * @return array
     */
    public static function toArray(ResolvableDtoInterface $dto): array
    {
        $array = $dto::getAttributeCollection()->getAssocAttributeArray($dto);
        return self::parseNested($array);
    }

    /**
     * Parses nested attributes to an array
     * @param array $array
     * @return array
     */
    private static function parseNested(array $array): array
    {
        $res = [];
        foreach ($array as $key => $item) {
            if (is_array($item)) {
                $res [$key] = self::parseNested($item);
            } elseif ($item instanceof ResolvableDtoInterface) {
                $res[$key] = self::toArray($item);
            } else {
                $res[$key] = $item;
            }
        }
        return $res;
    }

    /**
     * Finds an attribute in an associative array by key
     *
     * @param array $array
     * @param string $name
     * @return mixed|null
     */
    private static function findByKey(array $array, string $name)
    {
        if (array_key_exists($name, $array)) {
            return ($array[$name]);
        }
        return null;
    }

    /**
     * Resolves an attribute and assigns the value to the DTO
     *
     * @param DtoAttribute $attribute
     * @param $item
     * @param ResolvableDtoInterface $dto
     */
    private static function resolveAttribute(DtoAttribute $attribute, $item, ResolvableDtoInterface $dto): void
    {
        if ($attribute->isComplex()) {
            self::resolveComplexAttribute($attribute, $item, $dto);
        } else {
            self::resolvePrimitiveAttribute($attribute, $item, $dto);
        }
    }

    /**
     * Resolves a primitive attribute and assigns the value to the DTO
     *
     * @param DtoAttribute $attribute
     * @param $item
     * @param ResolvableDtoInterface $dto
     */
    private static function resolvePrimitiveAttribute(DtoAttribute $attribute, $item, ResolvableDtoInterface $dto): void
    {
        $attrName = $attribute->getName();
        $dto->$attrName = $item;
    }

    /**
     * Resolves s complex attribute and assigns the value to the DTO
     *
     * @param DtoAttribute $attribute
     * @param $item
     * @param ResolvableDtoInterface $dto
     */
    private static function resolveComplexAttribute(DtoAttribute $attribute, $item, ResolvableDtoInterface $dto): void
    {
        $attrName = $attribute->getName();

        if ($attribute->shouldParseObjectsFromArray()) {

            $innerArray = [];
            foreach ($item as $innerObj) {
                $innerArray[] = self::fromArray($innerObj, $attribute->getClassInstance());
            }
            $dto->$attrName = $innerArray;
        } else {
            $dto->$attrName = self::fromArray($item, $attribute->getClassInstance());
        }
    }
}