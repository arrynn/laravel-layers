<?php
declare(strict_types=1);

namespace Arrynn\Layers\TransferObjects;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use Arrynn\Layers\Services\Mapper\Mapper;

/**
 * Class AbstractCollectionDto
 * @package Arrynn\Layers\TransferObjects
 */
abstract class AbstractCollectionDto extends ResourceCollection
{

    /**
     * {@inheritDoc}
     */
    abstract public function getCollectionConfig(): CollectionDtoConfig;

    /**
     * AbstractCollectionDto constructor.
     * @param LengthAwarePaginator $paginator
     */
    public function __construct(LengthAwarePaginator $paginator)
    {
        parent::__construct($paginator);
    }

    /**
     * {@inheritDoc}
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($item) {
            $sourceObj = $item;
            $destObj = $this->getCollectionConfig()->getDestinationObj();
            $dto = Mapper::map($sourceObj, $destObj);
            return DtoResolver::toArray($dto);
        });
    }
}