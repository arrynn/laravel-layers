<?php
declare(strict_types=1);

namespace Arrynn\Layers\TransferObjects\Exceptions;

use Exception;

/**
 * Class DtoSettingException
 * @package Arrynn\Layers\TransferObjects\Exceptions
 */
class DtoSettingException extends Exception {

}