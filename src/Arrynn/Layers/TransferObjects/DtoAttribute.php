<?php
declare(strict_types=1);

namespace Arrynn\Layers\TransferObjects;

use Arrynn\Layers\TransferObjects\Contracts\ResolvableDtoInterface;
use Arrynn\Layers\TransferObjects\Exceptions\DtoSettingException;

class DtoAttribute
{
    const TYPE_PRIMITIVE = 'attr_type_primitive';
    const TYPE_COMPLEX = 'attr_type_complex';

    /**
     * @var string $name
     */
    private $name;
    /**
     * @var string $type
     */
    private $type;
    /**
     * @var null|ResolvableDtoInterface $class
     */
    private $class;
    /**
     * @var bool $shouldParseObjectsFromArray
     */
    private $shouldParseObjectsFromArray;

    /**
     * DtoAttribute constructor.
     *
     * @param $name
     * @param string $type
     * @param null|string $class
     * @param bool $shouldParseObjectsFromArray
     * @throws DtoSettingException
     */
    public function __construct($name, $type = self::TYPE_PRIMITIVE, $class = null, $shouldParseObjectsFromArray = false)
    {
        if ($type == self::TYPE_COMPLEX && !new $class() instanceof ResolvableDtoInterface) {
            throw new DtoSettingException("Complex DTO attributes must implement the ResolvableDtoInterface contract.");
        }

        $this->name = $name;
        $this->type = $type;
        $this->class = $class;
        $this->shouldParseObjectsFromArray = $shouldParseObjectsFromArray;
    }

    /**
     * Shows whether the attribute is complex
     *
     * @return bool
     */
    public function isComplex(): bool
    {
        return $this->type == self::TYPE_COMPLEX;
    }

    /**
     * Shows whether objects should be parsed from an array
     *
     * @return bool
     */
    public function shouldParseObjectsFromArray(): bool
    {
        return $this->shouldParseObjectsFromArray;
    }

    /**
     * Returns attribute name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Creates new dto instance
     *
     * @return ResolvableDtoInterface
     */
    public function getClassInstance(): ResolvableDtoInterface
    {
        return new $this->class();
    }
}