<?php
declare(strict_types=1);

namespace Arrynn\Layers\TransferObjects\Contracts;

/**
 * Interface IdentifiableDtoInterface
 * @package Arrynn\Layers\TransferObjects\Contracts
 */
interface IdentifiableDtoInterface
{
    /**
     * Returns unique identifier of a DTO
     *
     * @return mixed unique identifier
     */
    function getIdentity();
}