<?php
declare(strict_types=1);

namespace Arrynn\Layers\TransferObjects\Contracts;


use Arrynn\Layers\TransferObjects\DtoAttributeCollection;

/**
 * Interface ResolvableDtoInterface
 * @package Arrynn\Layers\TransferObjects\Contracts
 */
interface ResolvableDtoInterface
{
    /**
     * Returns an attribute collection
     *
     * @return DtoAttributeCollection
     */
    static function getAttributeCollection(): DtoAttributeCollection;
}