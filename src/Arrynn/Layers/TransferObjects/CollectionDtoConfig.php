<?php
declare(strict_types=1);


namespace Arrynn\Layers\TransferObjects;

/**
 * Class CollectionDtoConfig
 * @package Arrynn\Layers\TransferObjects
 */
class CollectionDtoConfig
{
    /**
     * @var string $sourceClass
     */
    private $sourceClass;
    /**
     * @var string $destinationClass
     */
    private $destinationClass;

    /**
     * CollectionDtoConfig constructor.
     *
     * @param string $sourceClass
     * @param string $destinationClass
     */
    private function __construct(string $sourceClass, string $destinationClass)
    {
        $this->sourceClass = $sourceClass;
        $this->destinationClass = $destinationClass;
    }

    /**
     * Creates a new instance of CollectionDtoConfig
     *
     * @param string $sourceClass
     * @param string $destinationClass
     * @return CollectionDtoConfig
     */
    public static function create(string $sourceClass, string $destinationClass): self
    {
        return new self($sourceClass, $destinationClass);
    }

    /**
     * Returns the source class
     *
     * @return string
     */
    public function getSourceClass(): string
    {
        return $this->sourceClass;
    }

    /**
     * Returns the destination class
     *
     * @return string
     */
    public function getDestinationClass(): string
    {
        return $this->destinationClass;
    }

    /**
     * Returns the source object
     *
     * @return object
     */
    public function getSourceObj()
    {
        return new $this->sourceClass();
    }

    /**
     * Returns the destination object
     *
     * @return object
     */
    public function getDestinationObj()
    {
        return new $this->destinationClass();
    }
}