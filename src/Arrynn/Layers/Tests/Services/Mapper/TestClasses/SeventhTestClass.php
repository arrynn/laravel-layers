<?php
declare(strict_types=1);

namespace Arrynn\Layers\Tests\Services\Mapper\TestClasses;


use Arrynn\Layers\Services\Mapper\Builder\MappingCollectionBuilder;
use Arrynn\Layers\Services\Mapper\Contracts\MappableInterface;
use Arrynn\Layers\Services\Mapper\Contracts\MappingCollectionInterface;

class SeventhTestClass implements MappableInterface
{
    public $firstAttributeDiff;
    public $secondAttributeDiff;
    /**
     * @var ThirdTestClass
     */
    public $thirdAttributeDiff;

    public function __construct($first = null, $second = null, $third = null)
    {
        $this->firstAttributeDiff = $first;
        $this->secondAttributeDiff = $second;
        $this->thirdAttributeDiff = $third;
    }

    public static function createEmpty()
    {
        return new self(null, null, null);
    }

    public static function createWithThirdAttrAs($third)
    {
        return new self(null, null, $third);
    }

    /**
     * MappingInterface collection to use with @see SecondTestClass.
     *
     * @return MappingCollectionInterface
     * @throws \Arrynn\Layers\Services\Mapper\Exceptions\MappingException
     */
    static function getMappingCollection(): MappingCollectionInterface
    {
        return MappingCollectionBuilder::create()
            ->addDirectMapping('firstAttributeDiff')
            ->addIndirectMapping('thirdAttribute', 'secondAttributeDiff')
            ->addNestedIndirectMapping('secondAttribute', 'thirdAttributeDiff', SecondTestClass::class, ThirdTestClass::class)
            ->build();
    }
}