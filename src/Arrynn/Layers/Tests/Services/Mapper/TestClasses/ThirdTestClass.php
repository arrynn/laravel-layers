<?php
declare(strict_types=1);

namespace Arrynn\Layers\Tests\Services\Mapper\TestClasses;


use Arrynn\Layers\Services\Mapper\Builder\MappingCollectionBuilder;
use Arrynn\Layers\Services\Mapper\Contracts\MappableInterface;
use Arrynn\Layers\Services\Mapper\Contracts\MappingCollectionInterface;

class ThirdTestClass implements MappableInterface
{

    public $firstAttribute;
    public $secondAttribute;
    public $thirdAttributeDifferent;

    public function __construct($first = null, $second = null, $third = null)
    {
        $this->firstAttribute = $first;
        $this->secondAttribute = $second;
        $this->thirdAttributeDifferent = $third;
    }

    public static function createEmpty()
    {
        return new self(null, null, null);
    }

    public static function createWithFirstAttrAs($first)
    {
        return new self($first);
    }

    public static function createWithEachAttrAs($each)
    {
        return new self($each, $each, $each);
    }

    public static function createWithEachAttrDifferent($prefix)
    {
        return new self($prefix.'_frst', $prefix.'_scnd', $prefix.'_thrd');
    }

    /**
     * MappingInterface collection to use with @see SecondTestClass with switched map roles (this class as target).
     *
     * @return MappingCollectionInterface
     */
    static function getMappingCollection(): MappingCollectionInterface
    {
        return MappingCollectionBuilder::create()
            ->addDirectMapping('firstAttribute')
            ->addIndirectMapping('secondAttributeDifferent', 'secondAttribute')
            ->addIndirectMapping('thirdAttribute', 'thirdAttributeDifferent')
            ->build();

    }
}