<?php
declare(strict_types=1);

namespace Arrynn\Layers\Tests\TransferObjects\TestClasses;

use Arrynn\Layers\Tests\Services\Mapper\TestClasses\TestCustomerModel;
use Arrynn\Layers\TransferObjects\AbstractCollectionDto;
use Arrynn\Layers\TransferObjects\CollectionDtoConfig;

/**
 * Class TestCustomerCollectionDto
 * @package Arrynn\Layers\Tests\TransferObjects\TestClasses
 */
class TestCustomerCollectionDto extends AbstractCollectionDto
{

    /**
     * Returns a CollectionDto configuration
     *
     * @return CollectionDtoConfig
     */
    public function getCollectionConfig(): CollectionDtoConfig
    {
        return CollectionDtoConfig::create(TestCustomerModel::class, TestSimplifiedResourceCustomerDto::class);
    }
}