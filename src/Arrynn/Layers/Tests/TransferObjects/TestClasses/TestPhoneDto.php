<?php
declare(strict_types=1);

namespace Arrynn\Layers\Tests\TransferObjects\TestClasses;


use Arrynn\Layers\Services\Mapper\Builder\MappingCollectionBuilder;
use Arrynn\Layers\Services\Mapper\Contracts\MappableInterface;
use Arrynn\Layers\Services\Mapper\Contracts\MappingCollectionInterface;
use Arrynn\Layers\TransferObjects\Builder\DtoAttributeCollectionBuilder;
use Arrynn\Layers\TransferObjects\Contracts\ResolvableDtoInterface;
use Arrynn\Layers\TransferObjects\DtoAttributeCollection;

/**
 * Class TestPhoneDto
 * @package Arrynn\MultilayeredInfrastructure\Tests\TransferObjects\TestClasses
 *
 * @property string $country_code
 * @property string $number
 */
class TestPhoneDto implements ResolvableDtoInterface, MappableInterface
{

    static function getAttributeCollection(): DtoAttributeCollection
    {
        return DtoAttributeCollectionBuilder::create()
            ->addPrimitive('country_code')
            ->addPrimitive('number')
            ->build();
    }

    public static function getFillExample()
    {
        return [
            'country_code' => '552',
            'number' => '454856214'
        ];
    }

    static function getMappingCollection(): MappingCollectionInterface
    {
        /**
         * mapping from @see TestAddressModel
         */
        return MappingCollectionBuilder::create()
            ->addDirectMapping('country_code')
            ->addDirectMapping('number')
            ->build();
    }
}