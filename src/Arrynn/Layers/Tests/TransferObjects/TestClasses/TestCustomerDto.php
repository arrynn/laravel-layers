<?php
declare(strict_types=1);

namespace Arrynn\Layers\Tests\TransferObjects\TestClasses;


use Arrynn\Layers\TransferObjects\Builder\DtoAttributeCollectionBuilder;
use Arrynn\Layers\TransferObjects\Contracts\ResolvableDtoInterface;
use Arrynn\Layers\TransferObjects\DtoAttributeCollection;

/**
 * Class TestCustomerDto
 * @package Arrynn\Layers\Tests\TransferObjects\TestClasses
 *
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property TestAddressDto $address
 * @property TestPhoneDto[] $phones
 */
class TestCustomerDto implements ResolvableDtoInterface
{

    static function getAttributeCollection(): DtoAttributeCollection
    {
        return DtoAttributeCollectionBuilder::create()
            ->addPrimitive('name')
            ->addPrimitive('surname')
            ->addPrimitive('email')
            ->addComplex('address', TestAddressDto::class)
            ->addComplexArray('phones', TestPhoneDto::class)
            ->build();
    }

    public static function getFillExample()
    {
        return [
            'name' => 'Robert',
            'surname' => 'Rowlon',
            'email' => 'rowlon@robrow.io',
            'address' => TestAddressDto::getFillExample(),
            'phones' => [TestPhoneDto::getFillExample()]
        ];
    }

    public static function getFillExample2()
    {
        $example = self::getFillExample();
        $phoneExample = TestPhoneDto::getFillExample();
        $phoneExample['number'] = '650548922';
        $example['phones'][] = $phoneExample;
        return $example;
    }
}