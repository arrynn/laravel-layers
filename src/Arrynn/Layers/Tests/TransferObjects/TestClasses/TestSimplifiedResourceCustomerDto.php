<?php
declare(strict_types=1);

namespace Arrynn\Layers\Tests\TransferObjects\TestClasses;

use Illuminate\Support\Carbon;
use Arrynn\Layers\Services\Mapper\Contracts\MappableInterface;
use Arrynn\Layers\Services\Mapper\Contracts\MappingCollectionInterface;
use Arrynn\Layers\Tests\Services\Mapper\TestClasses\TestAddressModel;
use Arrynn\Layers\Tests\Services\Mapper\TestClasses\TestCustomerModel;
use Arrynn\Layers\Tests\Services\Mapper\TestClasses\TestPhoneModel;
use Arrynn\Layers\TransferObjects\Builder\DtoAttributeCollectionBuilder;
use Arrynn\Layers\TransferObjects\Contracts\ResolvableDtoInterface;
use Arrynn\Layers\TransferObjects\DtoAttributeCollection;

/**
 * Class TestSimplifiedResourceCustomerDto
 * @package Arrynn\Layers\Tests\TransferObjects\TestClasses
 *
 * @property string $full_name
 * @property string $email
 * @property TestAddressDto $address
 * @property TestPhoneDto[] $phones
 * @property string $created_at
 */
class TestSimplifiedResourceCustomerDto implements MappableInterface, ResolvableDtoInterface
{

    static function getMappingCollection(): MappingCollectionInterface
    {
        return \Arrynn\Layers\Services\Mapper\Builder\MappingCollectionBuilder::create()
            ->addCustomMapping(self::getNamePropertyMapping())
            ->addDirectMapping('email')
            ->addNestedDirectMapping('address', TestAddressModel::class, TestAddressDto::class)
            ->addNestedDirectMapping('phones', TestPhoneModel::class, TestPhoneDto::class)
            ->addCustomMapping(self::getDatePropertyMapping('created_at'))
            ->build();
    }

    private static function getNamePropertyMapping()
    {
        /**
         * @param TestCustomerModel $source
         * @param TestSimplifiedResourceCustomerDto $target
         */
        return function ($source, $target) {
            $target->full_name = $source->name . ' ' . $source->surname;
        };
    }

    private static function getDatePropertyMapping($attrName)
    {
        /**
         * @param TestCustomerModel $source
         * @param TestSimplifiedResourceCustomerDto $target
         */
        return function ($source, $target) use ($attrName) {
            /**
             * @var Carbon $date
             */
            $date = $source->$attrName;
            $target->$attrName = Carbon::createFromFormat('Y-m-d H:i:s', $date, 'UTC')
                ->timezone('UTC')
                ->format('d. M Y H:i');
        };
    }


    static function getAttributeCollection(): DtoAttributeCollection
    {
        return DtoAttributeCollectionBuilder::create()
            ->addPrimitive('full_name')
            ->addPrimitive('email')
            ->addComplex('address', TestAddressDto::class)
            ->addComplexArray('phones', TestPhoneDto::class)
            ->addPrimitive('created_at')
            ->build();

    }
}