<?php
declare(strict_types=1);

namespace Arrynn\Layers\Tests\TransferObjects\TestClasses;

use Arrynn\Layers\Services\Mapper\Builder\MappingCollectionBuilder;
use Arrynn\Layers\Services\Mapper\Contracts\MappableInterface;
use Arrynn\Layers\Services\Mapper\Contracts\MappingCollectionInterface;
use Arrynn\Layers\TransferObjects\Builder\DtoAttributeCollectionBuilder;
use Arrynn\Layers\TransferObjects\Contracts\ResolvableDtoInterface;
use Arrynn\Layers\TransferObjects\DtoAttributeCollection;

/**
 * Class TestAddressDto
 * @package Arrynn\Layers\Tests\TransferObjects\TestClasses
 *
 * @property string $street
 * @property string $street_number
 * @property string $zip
 * @property string $city
 * @property string $country
 */
class TestAddressDto implements ResolvableDtoInterface, MappableInterface
{

    static function getAttributeCollection(): DtoAttributeCollection
    {
        return DtoAttributeCollectionBuilder::create()
            ->addPrimitive('street')
            ->addPrimitive('street_number')
            ->addPrimitive('zip')
            ->addPrimitive('city')
            ->addPrimitive('country')
            ->build();
    }

    public static function getFillExample()
    {
        return [
            'street' => 'Landside Rd.',
            'street_number' => '102c',
            'zip' => '1078 01',
            'city' => 'Worlhinki',
            'country' => 'Asgard'
        ];
    }

    static function getMappingCollection(): MappingCollectionInterface
    {
        /**
         * mapping from @see TestAddressModel
         */
        return MappingCollectionBuilder::create()
            ->addDirectMapping('street')
            ->addDirectMapping('street_number')
            ->addDirectMapping('zip')
            ->addDirectMapping('city')
            ->addDirectMapping('country')
            ->build();
    }
}