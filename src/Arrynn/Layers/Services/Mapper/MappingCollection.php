<?php declare(strict_types=1);

namespace Arrynn\Layers\Services\Mapper;

use Arrynn\Layers\Services\Mapper\Contracts\MappingInterface;
use Arrynn\Layers\Services\Mapper\Contracts\MappingCollectionInterface;

class MappingCollection implements MappingCollectionInterface
{

    /**
     * @var MappingInterface[]
     */
    private $mappings = [];

    /**
     * {@inheritDoc}
     */
    public function all(): array
    {
        return $this->mappings;
    }

    /**
     * {@inheritDoc}
     */
    public function add(MappingInterface $mapping)
    {
        $this->mappings[] = $mapping;
    }
}