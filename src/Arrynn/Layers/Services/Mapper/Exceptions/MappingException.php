<?php
declare(strict_types=1);

namespace Arrynn\Layers\Services\Mapper\Exceptions;

use Exception;

/**
 * Class MappingException
 * @package Arrynn\Layers\Services\Mapper\Exceptions
 */
class MappingException extends Exception {

}