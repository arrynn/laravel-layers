<?php
declare(strict_types=1);

namespace Arrynn\Layers\Services\Mapper\Helpers;

use Closure;
use Illuminate\Support\Carbon;

/**
 * Class MappingHelper
 * @package Arrynn\Layers\Services\Mapper\Helpers
 */
class MappingHelper
{
    /**
     * Creates a date mapping
     *
     * @param $attributeName
     * @param string $timezone
     * @return Closure
     */
    public static function dateMapping($attributeName, $timezone = 'UTC')
    {
        return self::indirectDateMapping($attributeName, $attributeName, $timezone);
    }

    /**
     * Creates an indirect date mapping
     *
     * @param $sourceAttributeName
     * @param $targetAttributeName
     * @param string $timezone
     * @param string $format
     * @return Closure
     */
    public static function indirectDateMapping($sourceAttributeName, $targetAttributeName, $timezone = 'UTC', $format = 'd. M. Y H:i')
    {
        return function ($source, $target) use ($sourceAttributeName, $targetAttributeName, $timezone, $format) {
            $attr = Carbon::parse($source->$sourceAttributeName);
            $target->$targetAttributeName = $attr->timezone($timezone)->format($format);
        };
    }

    /**
     * Creates an integer parsed mapping
     *
     * @param string $attributeName
     * @return Closure
     */
    static public function integerPriceMapping($attributeName = 'price')
    {
        return function ($source, $target) use ($attributeName) {
            $target->$attributeName = intval($source->$attributeName);
        };
    }
}