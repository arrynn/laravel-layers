<?php declare(strict_types=1);

namespace Arrynn\Layers\Services\Mapper;

use Arrynn\Layers\Services\Mapper\Contracts\MappableInterface;
use Arrynn\Layers\Services\Mapper\Exceptions\MappingException;

class Mapper
{
    /**
     * Maps the source object attributes to target object
     *
     * @param MappableInterface|mixed $source
     * @param MappableInterface|mixed $target
     * @return mixed
     * @throws MappingException if neither of the function call arguments
     * implement the MappableInterface interface.
     */
    public static function map($source, $target)
    {
        $contractHolder = $source;
        if (!$source instanceof MappableInterface) {
            if (!$target instanceof MappableInterface) {
                throw new MappingException("At least one of 
                provided objects for mapping must implement 
                the MappableInterface contract");
            }

            $contractHolder = $target;
        }

        $configuration = $contractHolder::getMappingCollection();
        $mappings = $configuration->all();
        foreach ($mappings as $mapping) {
            $closure = $mapping->getClosure();
            $closure($source, $target);
        }

        return $target;
    }
}