<?php declare(strict_types=1);

namespace Arrynn\Layers\Services\Mapper;

use Arrynn\Layers\Services\Mapper\Contracts\MappingInterface;
use Closure;

class Mapping implements MappingInterface
{
    /**
     * @var Closure $closure mapping closure
     */
    private $closure;

    /**
     * {@inheritDoc}
     */
    public function getClosure(): Closure
    {
        return $this->closure;
    }

    /**
     * {@inheritDoc}
     */
    public function setClosure(Closure $closure): void
    {
        $this->closure = $closure;
    }
}