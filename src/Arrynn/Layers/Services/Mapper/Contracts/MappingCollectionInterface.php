<?php
declare(strict_types=1);

namespace Arrynn\Layers\Services\Mapper\Contracts;

/**
 * Interface MappingCollectionInterface
 * @package Arrynn\Layers\Services\Mapper\Contracts
 */
interface MappingCollectionInterface
{
    /**
     * Returns all items of the mapping collection
     *
     * @return MappingInterface[]
     */
    public function all(): array;

    /**
     * Adds a mapping to a mapping collection
     *
     * @param MappingInterface $mapping
     * @return mixed
     */
    public function add(MappingInterface $mapping);
}