<?php
declare(strict_types=1);

namespace Arrynn\Layers\Services\Mapper\Contracts;

use Closure;

/**
 * Interface MappingInterface
 * @package Arrynn\Layers\Services\Mapper\Contracts
 */
interface MappingInterface
{
    /**
     * Returns the mapping closure function
     *
     * @return Closure
     */
    public function getClosure(): Closure;

    /**
     * Sets the mapping closure function
     *
     * @param Closure $closure
     */
    public function setClosure(Closure $closure): void;
}