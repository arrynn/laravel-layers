<?php
declare(strict_types=1);

namespace Arrynn\Layers\Services\Mapper\Contracts;

/**
 * Interface MappableInterface
 * @package Arrynn\Layers\Services\Mapper\Contracts
 */
interface MappableInterface
{
    /**
     * Returns a mapping collection
     *
     * @return MappingCollectionInterface
     */
    static function getMappingCollection(): MappingCollectionInterface;
}