<?php
declare(strict_types=1);

namespace Arrynn\Layers\Services\Mapper\Builder;


use Arrynn\Layers\Services\Mapper\Contracts\MappableInterface;
use Arrynn\Layers\Services\Mapper\Contracts\MappingCollectionInterface;
use Arrynn\Layers\Services\Mapper\Exceptions\MappingException;
use Arrynn\Layers\Services\Mapper\Mapper;
use Arrynn\Layers\Services\Mapper\Mapping;
use Arrynn\Layers\Services\Mapper\MappingCollection;
use Closure;
use Throwable;

/**
 * Class MappingCollectionBuilder
 * @package Arrynn\Layers\Services\Mapper\Builder
 */
class MappingCollectionBuilder
{
    /**
     * @var MappingCollection $collection
     */
    private $collection;

    /**
     * MappingCollectionBuilder constructor.
     */
    private function __construct()
    {
        $this->collection = new MappingCollection();
    }

    /**
     * Creates new builder instance
     *
     * @return MappingCollectionBuilder
     */
    public static function create()
    {
        return new self();
    }

    /**
     * Adds direct mapping.
     * Direct mapping maps an attribute from the source object
     * and maps it to an attribute with the same name in the target object
     *
     * @param string $attrName
     * @return MappingCollectionBuilder
     */
    public function addDirectMapping(string $attrName)
    {
        return $this->addIndirectMapping($attrName, $attrName);
    }

    /**
     * Adds indirect mapping.
     * Indirect mapping maps an attribute with the
     * name of $sourceAttr in source object and maps it
     * to an attribute with the $destinationAttr name in the target object
     *
     * @param string $sourceAttr
     * @param string $destinationAttr
     * @return $this
     */
    public function addIndirectMapping(string $sourceAttr, string $destinationAttr)
    {
        $mapping = new Mapping();
        $closure = function ($source, $target) use ($sourceAttr, $destinationAttr) {
            try {
                if(isset($source->$sourceAttr)) {
                    $target->$destinationAttr = $source->$sourceAttr;
                }
            } catch (Throwable $e) {
                throw new MappingException("There was an error during mapping of attribute '$sourceAttr'"
                    . "in MappableInterface class " . get_class($source) . "\n" . $e->getMessage(), 0, $e);
            }
        };
        $mapping->setClosure($closure);
        $this->collection->add($mapping);
        return $this;
    }

    /**
     * Adds nested direct maping.
     * Nested direct mapping maps an attribute from source object of $sourceObjectClass
     * to an attribute of the same name in target object of $destinationObjectClass.
     *
     *
     *
     * @param string $attrName
     * @param string $sourceObjectClass
     * @param string $destinationObjectClass
     * @return MappingCollectionBuilder
     * @throws MappingException
     */
    public function addNestedDirectMapping(string $attrName, string $sourceObjectClass, string $destinationObjectClass)
    {
        return $this->addNestedIndirectMapping($attrName, $attrName, $sourceObjectClass, $destinationObjectClass);
    }

    /**
     * Adds nested indirect maping.
     * Nested indirect mapping maps an attribute from source object of $sourceObjectClass
     * to an attribute of a different name in target object of $destinationObjectClass.
     *
     * @param string $sourceAttr
     * @param string $destinationAttr
     * @param string $sourceObjectClass
     * @param string $destinationObjectClass
     * @return $this
     * @throws MappingException
     */
    public function addNestedIndirectMapping(string $sourceAttr, string $destinationAttr, string $sourceObjectClass, string $destinationObjectClass)
    {
        $mapping = new Mapping();
        $sourceObj = new $sourceObjectClass();
        $destObj = new $destinationObjectClass();

        if (!$sourceObj instanceof MappableInterface && !$destObj instanceof MappableInterface) {
            throw new MappingException("Nested mapping requires that source or destination object classes implement the MappableInterface contract.");
        }
        $closure = function ($source, $target) use ($sourceAttr, $destinationAttr, $destObj) {
            try {
                if(is_array($source->$sourceAttr)){
                    // map all items of array to destObj
                    $array = [];
                    foreach($source->$sourceAttr as $sourceObj){
                        $mappedObj = Mapper::map($sourceObj, new $destObj());
                        $array[] = $mappedObj;
                    }
                    $target->$destinationAttr = $array;

                } else {
                    // only one destObj here
                    $mappedObj = Mapper::map($source->$sourceAttr, $destObj);
                    $target->$destinationAttr = $mappedObj;
                }
            } catch (Throwable $e) {
                throw new MappingException("There was an error during mapping of attribute '$sourceAttr'"
                    . "in MappableInterface class " . get_class($source) . "\n" . $e->getMessage(), 0, $e);
            }
        };
        $mapping->setClosure($closure);
        $this->collection->add($mapping);
        return $this;
    }

    /**
     * Adds closure-based mapping.
     * Takes a closure which takes two attributes, source object class and target object class.
     * The closure takes responsibility of the mapping.
     *
     * @param Closure $closure
     * @return $this
     */
    public function addCustomMapping(Closure $closure)
    {
        $mapping = new Mapping();
        $mapping->setClosure($closure);
        $this->collection->add($mapping);
        return $this;
    }

    /**
     * Builds the mapping collection
     *
     * @return MappingCollectionInterface
     */
    public function build(): MappingCollectionInterface
    {
        return $this->collection;
    }
}