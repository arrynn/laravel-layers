<?php declare(strict_types=1);

namespace Arrynn\Layers\Facades;

use Illuminate\Pagination\LengthAwarePaginator;
use Arrynn\Layers\Repositories\CrudRepositoryInterface;
use Arrynn\Layers\Services\Mapper\Mapper;
use Arrynn\Layers\TransferObjects\Contracts\IdentifiableDtoInterface;

/**
 * Class AbstractCrudFacade
 * @package Arrynn\Layers\Facades
 */
abstract class AbstractCrudFacade implements CrudFacadeInterface
{
    /**
     * @var CrudRepositoryInterface $repository
     */
    protected $repository;
    /**
     * @var object $modelContext
     */
    protected $modelContext;
    /**
     * @var object $indexCollectionDtoContext
     */
    protected $indexCollectionDtoContext;
    /**
     * @var object $detailDtoContext
     */
    protected $detailDtoContext;
    /**
     * @var object $createDtoContext
     */
    protected $createDtoContext;
    /**
     * @var object $updateDtoContext
     */
    protected $updateDtoContext;

    /**
     * Returns model context
     *
     * @return string model class
     */
    abstract protected function getModelContext();

    /**
     * Returns indexCollectionDto context
     *
     * @return string index collection dto class
     */
    abstract protected function getIndexCollectionDtoContext();

    /**
     * Returns detailDto context
     *
     * @return string detail dto class
     */
    abstract protected function getDetailDtoContext();

    /**
     * Returns createDto context
     *
     * @return string create dto class
     */
    abstract protected function getCreateDtoContext();

    /**
     * Returns updateDto context
     *
     * @return string updateDto class
     */
    abstract protected function getUpdateDtoContext();

    /**
     * AbstractCrudFacade constructor.
     *
     * @param CrudRepositoryInterface $repository
     */
    public function __construct(CrudRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->modelContext = $this->getModelContext();
        $this->indexCollectionDtoContext = $this->getIndexCollectionDtoContext();
        $this->detailDtoContext = $this->getDetailDtoContext();
        $this->createDtoContext = $this->getCreateDtoContext();
        $this->updateDtoContext = $this->getUpdateDtoContext();
    }


    /**
     * {@inheritDoc}
     */
    public function all()
    {
        return new $this->indexCollectionDtoContext($this->repository->all());
    }

    /**
     * {@inheritDoc}
     */
    public function get($key)
    {
        $model = $this->repository->get($key);
        $dto = Mapper::map($model, new $this->detailDtoContext());
        return $dto;
    }

    /**
     * {@inheritDoc}
     */
    public function create($dto)
    {
        $model = Mapper::map($dto, new $this->modelContext());
        $storedModel = $this->repository->create($model);
        $storedDto = Mapper::map($storedModel, new $this->detailDtoContext());
        return $storedDto;
    }

    /**
     * {@inheritDoc}
     */
    public function update(IdentifiableDtoInterface $dto)
    {
        $model = $this->repository->get($dto->getIdentity());
        $updatedModel = Mapper::map($dto, $model);
        $storedModel = $this->repository->update($updatedModel);
        $storedDto = Mapper::map($storedModel, new $this->detailDtoContext());
        return $storedDto;
    }

    /**
     * {@inheritDoc}
     */
    public function delete($key)
    {
        $this->repository->delete($key);
    }
}