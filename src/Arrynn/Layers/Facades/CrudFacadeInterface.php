<?php declare(strict_types=1);

namespace Arrynn\Layers\Facades;

use Arrynn\Layers\TransferObjects\Contracts\IdentifiableDtoInterface;

/**
 * Interface CrudFacadeInterface
 * @package Arrynn\Layers\Facades
 */
interface CrudFacadeInterface
{
    /**
     * Returns all objects of given context
     *
     * @return mixed
     */
    public function all();

    /**
     * Finds an object by primary key
     *
     * @param $key
     * @return mixed
     */
    public function get($key);

    /**
     * Stores new object
     *
     * @param $dto
     * @return mixed
     */
    public function create($dto);

    /**
     * Updates given object
     *
     * @param IdentifiableDtoInterface $dto
     * @return mixed
     */
    public function update(IdentifiableDtoInterface $dto);

    /**
     * Deletes an object
     *
     * @param $key
     * @return mixed
     */
    public function delete($key);
}