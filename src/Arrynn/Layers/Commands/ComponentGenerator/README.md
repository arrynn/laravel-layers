usage: `php artisan component:generate <dir> <componentName> <rootNamespace>`

    ------- Arguments -------
    dir             The directory in which the component will be placed (relative to app dir)
    componentName   Name of the generated component (Plural is used, e.g. Consultations )
    rootNamespace   The root namespace of the component 