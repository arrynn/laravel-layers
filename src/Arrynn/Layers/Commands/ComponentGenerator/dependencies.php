<?php
declare(strict_types=1);

use Arrynn\Layers\Facades\AbstractCrudFacade;
use Arrynn\Layers\Repositories\AbstractEloquentCrudRepository;
use Arrynn\Layers\Services\Mapper\Builder\MappingCollectionBuilder;
use Arrynn\Layers\Services\Mapper\Contracts\MappableInterface;
use Arrynn\Layers\Services\Mapper\Contracts\MappingCollectionInterface;
use Arrynn\Layers\TransferObjects\AbstractCollectionDto;
use Arrynn\Layers\TransferObjects\CollectionDtoConfig;
use Arrynn\Layers\TransferObjects\Contracts\ResolvableDtoInterface;
use Arrynn\Layers\TransferObjects\DtoAttributeCollection;
use Arrynn\Layers\TransferObjects\Builder\DtoAttributeCollectionBuilder;
use Arrynn\Layers\TransferObjects\DtoResolver;

return [
    'AbstractEloquentCrudRepository' => [
        'classPath' => AbstractEloquentCrudRepository::class,
        'class' => 'AbstractEloquentCrudRepository'
    ],
    'MappingCollectionBuilder' => [
        'classPath' => MappingCollectionBuilder::class,
        'class' => 'MappingCollectionBuilder'
    ],
    'MappableInterface' => [
        'classPath' => MappableInterface::class,
        'class' => 'MappableInterface'
    ],
    'ResolvableDtoInterface' => [
        'classPath' => ResolvableDtoInterface::class,
        'class' => 'ResolvableDtoInterface'
    ],
    'DtoAttributeCollectionBuilder' => [
        'classPath' => DtoAttributeCollectionBuilder::class,
        'class' => 'DtoAttributeCollectionBuilder'
    ],
    'DtoAttributeCollection' => [
        'classPath' => DtoAttributeCollection::class,
        'class' => 'DtoAttributeCollection'
    ],
    'MappingCollectionInterface' => [
        'classPath' => MappingCollectionInterface::class,
        'class' => 'MappingCollectionInterface'
    ],
    'AbstractCollectionDto' => [
        'classPath' => AbstractCollectionDto::class,
        'class' => 'AbstractCollectionDto'
    ],
    'CollectionDtoConfig' => [
        'classPath' => CollectionDtoConfig::class,
        'class' => 'CollectionDtoConfig'
    ],
    'AbstractCrudFacade' => [
        'classPath' => AbstractCrudFacade::class,
        'class' => 'AbstractCrudFacade'
    ],
    'DtoResolver' => [
        'classPath' => DtoResolver::class,
        'class' => 'DtoResolver'
    ],
];