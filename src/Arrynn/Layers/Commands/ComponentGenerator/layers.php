<?php declare(strict_types=1);

use Arrynn\Layers\TransferObjects\Contracts\IdentifiableDtoInterface;

return [
    'Models' => [
        'suffix' => '',
        'template' => 'model.php.template',
    ],
    'Repositories' => [
        'suffix' => 'Repository',
        'template' => 'repository.php.template',
    ],
    'Services' => [
        'suffix' => 'Service',
        'template' => 'service.php.template',
    ],
    'Facades' => [
        'suffix' => 'Facade',
        'template' => 'facade.php.template',
    ],
    'Providers' => [
        'suffix' => 'ServiceProvider',
        'template' => 'provider.php.template',
    ],
    'TransferObjects' => [
        'suffix' => 'Dto',
        'template' => 'dto.php.template',
        'generate' => [
            'signature' => 'createDtoFiles',
            'context' => [
                'template' => 'dto-collection.php.template',
                'suffix' => 'CollectionDto',
                'fileVariants' => [
                    'index' => [
                        'suffix' => 'Index'
                    ]
                ]
            ]
        ],
        'fileVariants' => [
            'create' => [
                'suffix' => 'Create'
            ],
            'update' => [
                'suffix' => 'Update',
                'iIdentifiableClassPath' => 'use ' . IdentifiableDtoInterface::class.';',
                'iIdentifiableClassImplement' => ', IdentifiableDtoInterface',
                'iIdentifiableMethod' => "public function getIdentity(){\n\t\treturn \$this->id;\n\t}",
            ],
            'detail' => [
                'suffix' => 'Detail'
            ],
            'index' => [
                'suffix' => 'Index'
            ]
        ],
    ],
    'Controllers' => [
        'suffix' => 'Controller',
        'template' => 'controller.php.template',
        'fileVariants' => [
            'api' => [
                'suffix' => 'Api'
            ]
        ]
    ],
    'Requests' => [
        'suffix' => 'Request',
        'template' => 'request.php.template',
        'fileVariants' => [
            'create' => [
                'suffix' => 'Create'
            ],
            'update' => [
                'suffix' => 'Update'
            ],
        ],
    ]
];