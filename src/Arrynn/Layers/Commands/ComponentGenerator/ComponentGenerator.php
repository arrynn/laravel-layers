<?php declare(strict_types=1);

namespace Arrynn\Layers\Commands\ComponentGenerator;

use Illuminate\Console\Command;

/**
 * Class ComponentGenerator
 * @package Arrynn\Layers\Commands\ComponentGenerator
 */
class ComponentGenerator extends Command
{
    const FILE_EXTENSION = '.php';
    const NAMESPACE_SEPARATOR = '\\';
    const ARCHITECTURE_NAMESPACE = 'Arrynn\\Layers';
    const TEMPLATE_DIR = 'templates';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'component:generate {dir} {component} {namespace}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate a component';

    /**
     * The root directory of the component relative to app directory
     *
     * @var string
     */
    private $dir;

    /**
     * Component name
     *
     * @var string
     */
    private $component;

    /**
     * Component base namespace
     *
     * @var string
     */
    private $namespace;

    /**
     * Application layer configuration
     *
     * @var array $layers
     */
    private $layers;


    /**
     * List of infrastructure dependencies
     *
     * @var array $dependencies
     */
    private $dependencies;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->layers = require('layers.php');
        $this->dependencies = require('dependencies.php');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dir = $this->argument('dir');
        $this->component = $this->argument('component');
        $this->namespace = $this->argument('namespace');

        $this->makeDirectories();
    }

    /**
     * Creates directory structure for component layers
     */
    private function makeDirectories()
    {
        $entryDir = $this->dir . DIRECTORY_SEPARATOR . $this->component;
        $dirPath = app_path($entryDir);
        if (!is_dir($dirPath)) {
            $this->makeComponentDir($dirPath);
            $this->prepareComponentLayers($dirPath);
            $this->createFiles();
        } else {
            echo 'Specified directory already exists.';
        }
    }

    /**
     * Creates component root directory
     *
     * @param string $dirPath
     */
    private function makeComponentDir(string $dirPath)
    {
        $success = mkdir($dirPath, 0777, true);
        if (!$success) {
            die('Attept to create component dir failed.');
        }
    }

    /**
     * Prepares component layers
     *
     * @param string $dirPath
     */
    private function prepareComponentLayers(string $dirPath)
    {
        foreach (array_keys($this->layers) as $layerName) {
            $layerDirPath = $dirPath . DIRECTORY_SEPARATOR . $layerName;
            $this->makeComponentDir($layerDirPath);
            $this->setLayerContext($layerName, $layerDirPath);
        }
        $this->setDtoLayerSubContext($this->layers);
    }

    /**
     * Inserts additional context data to layer configuration
     *
     * @param string $layerName
     * @param string $layerDirPath
     */
    private function setLayerContext(string $layerName, string $layerDirPath)
    {
        $layer = &$this->layers[$layerName];
        $layer['dirPath'] = $layerDirPath;
        $layerNamespace = $this->namespace . self::NAMESPACE_SEPARATOR . $layerName;
        $layer['namespace'] = $layerNamespace;

        if (!isset($layer['fileVariants'])) {
            $fileName = str_singular($this->component) . $layer['suffix'];
            $file = $fileName . self::FILE_EXTENSION;
            $layer['filePath'] = $layerDirPath . DIRECTORY_SEPARATOR . $file;
            $layer['classPath'] = $layerNamespace . self::NAMESPACE_SEPARATOR . $fileName;
            $layer['class'] = $fileName;
            $layer['classWithoutSuffix'] = str_singular($this->component);
            $layer['classWithoutSuffixPlural'] = $this->component;
        } else {
            foreach ($layer['fileVariants'] as $variantKey => $variantContext) {
                $variant = &$layer['fileVariants'][$variantKey];
                $fileName = str_singular($this->component) . $variant['suffix'] . $layer['suffix'];
                $file = $fileName . self::FILE_EXTENSION;
                $variant['filePath'] = $layerDirPath . DIRECTORY_SEPARATOR . $file;
                $variant['classPath'] = $layerNamespace . self::NAMESPACE_SEPARATOR . $fileName;
                $variant['class'] = $fileName;
                $variant['classWithoutSuffix'] = str_singular($this->component);
                $variant['classWithoutSuffixPlural'] = $this->component;
            }
        }
    }

    /**
     * Creates files for all layers in configuration
     */
    private function createFiles()
    {
        foreach ($this->layers as $layerName => $layerContext) {
            $this->createLayerFiles($layerContext);
        }
    }

    /**
     * Creates files for component layer according to context configuration
     *
     * @param array $context
     */
    private function createLayerFiles(array $context)
    {
        if (isset($context['generate'])) {
            $generateMethod = $context['generate']['signature'];
            call_user_func_array(self::class . '::' . $generateMethod, ['context' => $context]);
        } else {
            $this->createDefaultLayerFiles($context);
        }
    }

    /**
     * Creates files for DTOs
     *
     * @param array $context
     */
    private function createDtoFiles(array $context)
    {
        $this->createDefaultLayerFiles($context);
        $indexContext = $this->layers['TransferObjects']['generate']['context'];
        $this->createDefaultLayerFiles($indexContext);
    }

    /**
     * Inserts additional context data to DTO layer configuration
     */
    private function setDtoLayerSubContext()
    {
        $context = $this->layers['TransferObjects'];
        $indexContext = $context['generate']['context'];
        $indexContext['dirPath'] = $context['dirPath'];
        $indexContext['namespace'] = $context['namespace'];
        $fileName = str_singular($this->component)
            . $indexContext['fileVariants']['index']['suffix']
            . $indexContext['suffix'];
        $file = $fileName . self::FILE_EXTENSION;
        $indexContext['fileVariants']['index']['filePath'] = $context['dirPath']
            . DIRECTORY_SEPARATOR . $file;
        $indexContext['fileVariants']['index']['classPath'] = $indexContext['namespace']
            . self::NAMESPACE_SEPARATOR . $fileName;

        $indexContext['fileVariants']['index']['class'] = $fileName;
        $this->layers['TransferObjects']['generate']['context'] = $indexContext;
    }

    /**
     * Creates layer component files.
     *
     * @param array $context
     */
    private function createDefaultLayerFiles(array $context)
    {
        $templateFilePath = dirname(__FILE__)
            . DIRECTORY_SEPARATOR . self::TEMPLATE_DIR
            . DIRECTORY_SEPARATOR . $context['template'];
        $template = file_get_contents($templateFilePath);

        if (!isset($context['fileVariants'])) {
            $this->createFile($template, $context);
        } else {
            foreach ($context['fileVariants'] as $variantKey => $variantContext) {
                $variantContext['namespace'] = $context['namespace'];
                $this->createFile($template, $variantContext);
            }
        }
    }

    /**
     * Saves layer component with assigned template to a file.
     *
     * @param string $template
     * @param array $context
     */
    private function createFile(string $template, array $context)
    {
        $processedTemplate = $this
            ->replaceVariables($template, 'context', $context);
        $processedTemplate = $this
            ->replaceVariables($processedTemplate, 'relativeDependency', $this->layers);
        $processedTemplate = $this
            ->replaceVariables($processedTemplate, 'dependency', $this->dependencies);

        echo "Creating ".$context['class']." class;\n";

        $file = fopen($context['filePath'], "w");
        fwrite($file, $processedTemplate);
        fclose($file);
    }

    /**
     * Replaces variables in templates with assigned values from the context.
     *
     * @param string $template
     * @param string $variableType
     * @param array $values
     * @return string template with filled variables
     */
    private function replaceVariables(string $template, string $variableType, array $values)
    {
        $matches = [];
        $regex = '/<@-' . $variableType . ':(\S+)-@>/i';

        preg_match_all($regex, $template, $matches);
        $matchedGroups = $matches[0];
        $matchedValues = $matches[1];

        if (empty($matchedGroups)) {
            return $template;
        }

        foreach ($matchedGroups as $key => $occurence) {
            $value = array_get($values, $matchedValues[$key], '');
            $template = str_replace($occurence, $value, $template);
        }
        return $template;
    }
}